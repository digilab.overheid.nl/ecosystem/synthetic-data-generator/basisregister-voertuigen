-- name: VehicleListAsc :many
SELECT v.id, v.license_plate, v.brand, v.internal_id FROM (
SELECT id, license_plate, brand, internal_id
FROM digilab_demo_frv.vehicles
WHERE vehicles.internal_id > (
    SELECT sub_qry_people.internal_id
    FROM digilab_demo_frv.vehicles as sub_qry_people
    WHERE sub_qry_people.id=$1
)
ORDER BY vehicles.internal_id ASC
LIMIT $2
) v order by v.internal_id DESC;

-- name: VehicleListDesc :many
SELECT id, license_plate, brand, internal_id
FROM digilab_demo_frv.vehicles
WHERE vehicles.internal_id < COALESCE((
    SELECT sub_qry_people.internal_id
    FROM digilab_demo_frv.vehicles as sub_qry_people
    WHERE sub_qry_people.id=$1
), $2)
ORDER BY vehicles.internal_id DESC
LIMIT $3;

-- name: VehicleGet :one
SELECT id, license_plate, brand, internal_id
FROM digilab_demo_frv.vehicles
WHERE id=$1;

-- name: VehicleOwners :many
SELECT person_id, assigned_at
FROM digilab_demo_frv.private_owner
WHERE vehicle_id=$1;

-- name: PeopleVehicles :many
SELECT id, license_plate, brand, internal_id
FROM digilab_demo_frv.vehicles
LEFT JOIN digilab_demo_frv.private_owner as owner
    ON owner.person_id=$1
WHERE digilab_demo_frv.vehicles.id=owner.vehicle_id;

-- name: VehicleCreate :exec
INSERT INTO digilab_demo_frv.vehicles (
    id, license_plate, brand
)
VALUES ($1, $2, $3);

-- name: VehiclePurchase :exec
INSERT INTO digilab_demo_frv.private_owner (
    person_id, vehicle_id, assigned_at
)
VALUES ($1, $2, $3);
