BEGIN TRANSACTION;

DROP INDEX digilab_demo_frv.idx_people_bsn;

ALTER TABLE digilab_demo_frv.private_owner DROP CONSTRAINT fk_private_owner_person_id;

DROP TABLE digilab_demo_frv.people;

COMMIT;
