BEGIN transaction;

CREATE SCHEMA digilab_demo_frv;

CREATE TABLE digilab_demo_frv.vehicles (
    id UUID NOT NULL,
    license_plate TEXT UNIQUE,
    brand TEXT,

    PRIMARY KEY(id)
);

CREATE TABLE digilab_demo_frv.people (
    id UUID NOT NULL,
    bsn TEXT UNIQUE,

    PRIMARY KEY(id)
);

CREATE TABLE digilab_demo_frv.private_owner (
    person_id UUID NOT NULL,
    vehicle_id UUID NOT NULL,
    assigned_at TIMESTAMP,

    CONSTRAINT fk_private_owner_person_id FOREIGN KEY(person_id) REFERENCES digilab_demo_frv.people(id),
    CONSTRAINT fk_private_owner_vehicle_id FOREIGN KEY(vehicle_id) REFERENCES digilab_demo_frv.vehicles(id)
);

CREATE INDEX idx_people_bsn ON digilab_demo_frv.people(bsn);
CREATE INDEX idx_private_owner_person_id ON digilab_demo_frv.private_owner(person_id);
CREATE INDEX idx_private_owner_vehicle_id ON digilab_demo_frv.private_owner(vehicle_id);

COMMIT;
