package adapter

import (
	"database/sql"

	"github.com/google/uuid"
	"gitlab.com/digilab.overheid.nl/synthetic-data-generator/fictief-register-voertuigen/backend/pkg/model"
	queries "gitlab.com/digilab.overheid.nl/synthetic-data-generator/fictief-register-voertuigen/backend/pkg/storage/queries/generated"
)

func ToVehicle(record *queries.DigilabDemoFrvVehicle) (*model.Vehicle, error) {
	return &model.Vehicle{
		ID:           record.ID,
		LicensePlate: record.LicensePlate.String,
		Brand:        record.Brand.String,
	}, nil
}

func ToVehicles(records []*queries.DigilabDemoFrvVehicle) ([]model.Vehicle, error) {
	vehicles := make([]model.Vehicle, 0, len(records))

	for idx := range records {
		vehicle, err := ToVehicle(records[idx])
		if err != nil {
			return nil, err
		}

		vehicles = append(vehicles, *vehicle)
	}

	return vehicles, nil
}

func FromVehicleCreate(vehicle *model.Vehicle) *queries.VehicleCreateParams {
	return &queries.VehicleCreateParams{
		ID:           vehicle.ID,
		LicensePlate: sql.NullString{String: vehicle.LicensePlate, Valid: true},
		Brand:        sql.NullString{String: vehicle.Brand, Valid: true},
	}
}

func FromPrivateOwner(id uuid.UUID, owner *model.PrivateOwner) *queries.VehiclePurchaseParams {
	return &queries.VehiclePurchaseParams{
		VehicleID:  id,
		PersonID:   owner.PersonID,
		AssignedAt: sql.NullTime{Time: owner.AssignedAt, Valid: true},
	}
}
