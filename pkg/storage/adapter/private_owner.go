package adapter

import (
	"gitlab.com/digilab.overheid.nl/synthetic-data-generator/fictief-register-voertuigen/backend/pkg/model"
	queries "gitlab.com/digilab.overheid.nl/synthetic-data-generator/fictief-register-voertuigen/backend/pkg/storage/queries/generated"
)

func ToPrivateOwner(record *queries.VehicleOwnersRow) (*model.PrivateOwner, error) {
	owner := &model.PrivateOwner{
		PersonID: record.PersonID,
	}

	if record.AssignedAt.Valid {
		owner.AssignedAt = record.AssignedAt.Time
	}

	return owner, nil
}

func ToPrivateOwners(record []*queries.VehicleOwnersRow) ([]model.PrivateOwner, error) {
	owners := make([]model.PrivateOwner, 0, len(record))

	for idx := range record {
		owner, err := ToPrivateOwner(record[idx])
		if err != nil {
			return nil, err
		}

		owners = append(owners, *owner)
	}

	return owners, nil
}
