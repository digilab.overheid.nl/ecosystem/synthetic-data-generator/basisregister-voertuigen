package model

import (
	"time"

	"github.com/google/uuid"
)

type Vehicle struct {
	ID           uuid.UUID      `json:"id"`
	LicensePlate string         `json:"licensePlate,omitempty"`
	Brand        string         `json:"brand,omitempty"`
	Owners       []PrivateOwner `json:"owners,omitempty"`
}

func (vehicle Vehicle) GetID() uuid.UUID {
	return vehicle.ID
}

type PrivateOwner struct {
	PersonID   uuid.UUID `json:"personId"`
	AssignedAt time.Time `json:"assignedAt"`
}
