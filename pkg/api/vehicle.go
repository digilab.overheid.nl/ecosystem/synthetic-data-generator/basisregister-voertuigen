//nolint:nlreturn // Makes code more unreadable
package api

import (
	"context"
	"encoding/json"
	"fmt"
	"math"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/google/uuid"
	"gitlab.com/digilab.overheid.nl/synthetic-data-generator/fictief-register-voertuigen/backend/pkg/meta"
	"gitlab.com/digilab.overheid.nl/synthetic-data-generator/fictief-register-voertuigen/backend/pkg/model"
	"gitlab.com/digilab.overheid.nl/synthetic-data-generator/fictief-register-voertuigen/backend/pkg/pagination"
	"gitlab.com/digilab.overheid.nl/synthetic-data-generator/fictief-register-voertuigen/backend/pkg/storage/adapter"
	queries "gitlab.com/digilab.overheid.nl/synthetic-data-generator/fictief-register-voertuigen/backend/pkg/storage/queries/generated"
)

func (a *API) VehicleList(w http.ResponseWriter, r *http.Request) {
	md, err := meta.New(r)
	if err != nil {
		writeError(w, err)
		return
	}

	var records []*queries.DigilabDemoFrvVehicle

	switch md.IsASC() {
	case true:
		records, err = a.vehicleListAsc(r.Context(), md)
	case false:
		records, err = a.vehicleListDesc(r.Context(), md)
	}

	if err != nil {
		writeError(w, err)
		return
	}

	vehicles, err := adapter.ToVehicles(records)
	if err != nil {
		writeError(w, err)
		return
	}

	response, err := pagination.Build(vehicles, md)
	if err != nil {
		writeError(w, err)
		return
	}

	if err := json.NewEncoder(w).Encode(response); err != nil {
		writeError(w, err)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func (a *API) vehicleListAsc(ctx context.Context, md *meta.Data) ([]*queries.DigilabDemoFrvVehicle, error) {
	params := &queries.VehicleListAscParams{
		ID:    md.FirstID,
		Limit: int32(md.PerPage + 1),
	}

	records, err := a.db.Queries.VehicleListAsc(ctx, params)
	if err != nil {
		return nil, fmt.Errorf("vehicle list failed: %w", err)
	}

	return records, nil
}

func (a *API) vehicleListDesc(ctx context.Context, md *meta.Data) ([]*queries.DigilabDemoFrvVehicle, error) {
	params := &queries.VehicleListDescParams{
		ID:         md.LastID,
		InternalID: math.MaxInt32,
		Limit:      int32(md.PerPage + 1),
	}

	records, err := a.db.Queries.VehicleListDesc(ctx, params)
	if err != nil {
		return nil, fmt.Errorf("vehicle list failed: %w", err)
	}

	return records, nil
}

func (a *API) VehicleGet(w http.ResponseWriter, r *http.Request) {
	id, err := uuid.Parse(chi.URLParam(r, "id"))
	if err != nil {
		writeError(w, err)
		return
	}

	record, err := a.db.Queries.VehicleGet(r.Context(), id)
	if err != nil {
		writeError(w, err)
		return
	}

	ownerRecords, err := a.db.Queries.VehicleOwners(r.Context(), id)
	if err != nil {
		writeError(w, err)
		return
	}

	vehicle, err := adapter.ToVehicle(record)
	if err != nil {
		writeError(w, err)
		return
	}

	if vehicle.Owners, err = adapter.ToPrivateOwners(ownerRecords); err != nil {
		writeError(w, err)
		return
	}

	if err := json.NewEncoder(w).Encode(vehicle); err != nil {
		writeError(w, err)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func (a *API) VehicleCreate(w http.ResponseWriter, r *http.Request) {
	vehicle := &model.Vehicle{ID: uuid.New()}
	if err := json.NewDecoder(r.Body).Decode(vehicle); err != nil {
		writeError(w, err)
		return
	}

	if err := a.db.Queries.VehicleCreate(r.Context(), adapter.FromVehicleCreate(vehicle)); err != nil {
		writeError(w, err)
		return
	}

	w.WriteHeader(http.StatusCreated)
}

func (a *API) VehiclePurchase(w http.ResponseWriter, r *http.Request) {
	id, err := uuid.Parse(chi.URLParam(r, "id"))
	if err != nil {
		writeError(w, err)
		return
	}

	owner := &model.PrivateOwner{}
	if err := json.NewDecoder(r.Body).Decode(owner); err != nil {
		writeError(w, err)
		return
	}

	if err := a.db.Queries.VehiclePurchase(r.Context(), adapter.FromPrivateOwner(id, owner)); err != nil {
		writeError(w, err)
		return
	}

	w.WriteHeader(http.StatusCreated)
}
