//nolint:nlreturn // Makes code more unreadable
package api

import (
	"encoding/json"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/google/uuid"
	"gitlab.com/digilab.overheid.nl/synthetic-data-generator/fictief-register-voertuigen/backend/pkg/storage/adapter"
)

func (a *API) PeopleVehicles(w http.ResponseWriter, r *http.Request) {
	personID, err := uuid.Parse(chi.URLParam(r, "personID"))
	if err != nil {
		writeError(w, err)
		return
	}

	records, err := a.db.Queries.PeopleVehicles(r.Context(), personID)
	if err != nil {
		writeError(w, err)
		return
	}

	vehicles, err := adapter.ToVehicles(records)
	if err != nil {
		writeError(w, err)
		return
	}

	if err := json.NewEncoder(w).Encode(vehicles); err != nil {
		writeError(w, err)
		return
	}

	w.WriteHeader(http.StatusOK)
}
