package cmd

import "github.com/spf13/cobra"

var RootCmd = &cobra.Command{ //nolint:gochecknoglobals // this is the recommended way to use cobra
	Use:   "api",
	Short: "frv API",
	Long:  "Fictief register voertuigen API",
}

func Execute() error {
	return RootCmd.Execute() //nolint:wrapcheck // not necessary
}

func init() { //nolint:gochecknoinits // this is the recommended way to use cobra
	RootCmd.AddCommand(serveCommand)
	RootCmd.AddCommand(migrateCommand)
}
